# -*- coding: utf-8 -*-
"""
Created on Sat Feb 10 13:23:27 2018

@author: Indu
"""

import manynames

x = 66
print x                     #66: global here
print manynames.x           #11: globals become attributes after import

manynames.f()               #11: manyname's x, not the one here
manynames.g()               #22: local in other's file's function

print manynames.C.x         #33: attribute of class in other module
I = manynames.C()
print I.x                   #33: still from class here
I.m()
print I.x                   #55: now from instance