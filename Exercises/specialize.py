# -*- coding: utf-8 -*-
"""
Created on Fri Feb 09 21:26:34 2018

@author: Indu
"""
#from abc import ABCMeta, abstractmethod

class Super:
#    __metaclass__ = ABCMeta
    def method(self):
        print('in super.method')
    def delegate(self):
        self.action()
#    @abstractmethod
    def action(self):
        raise NotImplementedError('action must be defined!!')
    
class Inheritor(Super):
    pass

class Replacer(Super):
    def method(self):
        print('in replacer.method')
        
class Extender(Super):
    def method(self):
        print('starting extender.method')
        Super.method(self)
        print('ending extender method')
        
class Provider(Super):
    def action(self):
        print('in provider.action')
       
class Sub(Super):
    def action(self):
        print('spam')

if __name__ == '__main__':
    for klass in (Inheritor, Replacer, Extender):
        print('\n' + klass.__name__ + '...')
        klass().method()
    print('\nProvider...')
    x = Sub()
    x.delegate()