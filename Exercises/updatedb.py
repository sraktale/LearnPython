# -*- coding: utf-8 -*-
"""
Created on Thu Feb 08 12:41:27 2018

@author: Indu
"""

import shelve
db = shelve.open('persondb')

for key in sorted(db):
    print key, '\t=>', db[key]

sue = db['Sue Jones']
sue.giveRaise(.10)
db['Sue Jones'] = sue
db.close()