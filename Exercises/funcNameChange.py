# -*- coding: utf-8 -*-
"""
Created on Sat Feb 10 14:53:39 2018

@author: Indu
"""
x = 11      # Global in module

def g1():
    print x     # Reference global in module
    
def g2():
    global x
    x = 22      # Change global in module
    
def h1():
    x = 33      # Local in function
    print x
    def nested():
        print x     # Reference local in enclosing scope
 
'''
def h2():
    x = 33
    def nested():
        nonlocal x
        x = 44
'''
    
g2()
g1()
