# -*- coding: utf-8 -*-
"""
Created on Sat Feb 10 16:48:42 2018

@author: Indu
"""

"I am: docstr.__doc__"

def func(args):
    "I am: docstr.func.__doc__"
    pass

class spam:
    "I am: spam.__doc__ or docstr.spam.__doc__"
    def method(self, arg):
        "I am: spam.method.__doc__ or self.method.__doc__"
        pass
