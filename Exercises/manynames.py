# -*- coding: utf-8 -*-
"""
Created on Sat Feb 10 11:17:37 2018

@author: Indu
"""
x = 11      # Global (module) name/attribute (x or manynames.x)

def f():    # Access globa x
    print x

def g():    # Local (function) variable (x, hides module x)
    x = 22
    print x
    
class C:
    x = 33  # Class attribute (C.x)
    def m(self):
        x = 44  # Local attribute in method(x)
        self.x = 55 # Instance attribute (instance.x)
        
if __name__ == '__main__':
    print(x) # 11: module(aka manynames.x outside file)
    f() # 11: global
    g() # 22: local
    print(x) # 11: module name unchanged
    
    obj = C() # Make instance
    print obj.x # Class name inherited by instance
    
    obj.m() # Attach attribute name x to instance now
    print(obj.x) #55 : instance
    print(C.x) # class (obj x if no x in instance)
    
    #print(C.m.x) # Fails: Only visible in method
    #print(g.x) # Fails: Only visible in function