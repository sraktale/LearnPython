# -*- coding: utf-8 -*-
"""
Created on Wed Feb 07 18:43:19 2018

@author: Indu
"""

"Assorted class utilities and tools"

class AttrDisplay:
    def __gatherAttrs(self):
        attrs = []
        for key in sorted(self.__dict__):
            attrs.append('%s=%s' % (key, getattr(self, key)))
        return ', '.join(attrs)
    def __str__(self):
        return '[%s: %s]' % (self.__class__.__name__, self.__gatherAttrs())
    
    
if __name__ == '__main__':
    class TopTest(AttrDisplay):
        count = 0
        def __init__(self):
            self.attr1 = TopTest.count
            self.attr2 = TopTest.count + 1
            TopTest.count += 2
        def __gatherAttrs(self):
            return 'Spam'
            
    class SubTest(TopTest):
        pass
    
    
    x, y = TopTest(), SubTest()
    print(x)
    print(y)