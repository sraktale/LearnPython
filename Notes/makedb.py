# -*- coding: utf-8 -*-
"""
Created on Thu Feb 08 09:20:47 2018

@author: Indu
"""

from person import Person, Manager

bob = Person('Bob Smith')
sue = Person('Sue Jones', job='dev', pay=100000)
tom = Manager('Tom Jones', 50000)

import shelve
db = shelve.open('persondb')
for object in (bob, sue, tom):
    db[object.name] = object
db.close()
