# -*- coding: utf-8 -*-
"""
Created on Tue Feb 20 16:23:47 2018

@author: Indu
"""

import numpy as np
import numpy.testing as npt
import pytest
import tic_tac_toe as game


@pytest.fixture
def letter():
    return 'O'

def test_to_check_not_winner(letter):
    print('test_to_check_not_winner')
    board = np.array([['X', '', ''], ['X', '', 'O'], ['', 'O', 'X']])
    npt.assert_equal(game.isWinner(board, letter), False)
    
def test_to_check_winner_in_rows(letter):
    board = np.array([['X', 'O', 'X'], ['X', 'X', 'O'], ['O', 'O', 'O']])
    npt.assert_equal(game.isWinner(board, letter), True)
    
def test_to_check_winner_in_columns(letter):
    board = np.array([['X', 'O', 'X'], ['X', 'O', 'X'], ['O', 'O', 'O']])
    npt.assert_equal(game.isWinner(board, letter), True)
    
def test_to_check_winner_in_diagonals(letter):
    board = np.array([['O', 'O', 'X'], ['X', 'O', 'X'], ['X', 'O', 'O']])
    npt.assert_equal(game.isWinner(board, letter), True)    

def test_to_check_validation_for_2D_board(letter):
    board = np.array([[['X'], ['O']], [['X'], ['O']]])
    npt.assert_raises(Exception, game.isWinner, board, letter)
    
def test_to_check_for_square_board(letter):
    board = np.array([['X', '', ''], ['', 'O', '']])
    npt.assert_raises(Exception, game.isWinner, board, letter)
    
def test_to_check_for_valid_marks_on_board(letter):
    board = np.array([['X', '', 'S'], ['X', '', 'O'], ['', 'O', 'X']])
    npt.assert_raises(Exception, game.isWinner, board, letter)
 
def test_to_check_for_valid_mark():
    board = np.array([['X', '', ''], ['X', '', 'O'], ['', 'O', 'X']])
    npt.assert_raises(Exception, game.isWinner, board, 'S')    