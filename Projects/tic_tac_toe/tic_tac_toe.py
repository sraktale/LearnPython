# -*- coding: utf-8 -*-
"""
Created on Tue Feb 20 16:41:18 2018

@author: Indu
"""
import numpy as np

def displayBoard(board):
    # Display the board in its current state.
    print(board)  

def _checkRows(bo, le):    
    for i in range(0, bo.shape[0]):
        rows_win = (bo[i, :] == le).all()
        if rows_win:
            return True
 
def _checkColumns(bo, le):    
    for i in range(0, bo.shape[0]):
        cols_win = (bo[i, :] == le).all()
        if cols_win:
            return True
        
def _checkDiagonals(bo, le):
    diag1_win = (np.diag(bo) == le).all()
    diag2_win = (np.diag(np.fliplr(bo)) == le).all()
    if diag1_win or diag2_win:
        return True
    
def _validate(bo, le):
    if bo.ndim != 2:
        raise Exception('Game can handle only 2-dimenstional boards') 
    if bo.shape[0] != bo.shape[1]:
        raise Exception("Game can handle only square boards")
    if le not in ['X', 'O', '']:
        raise Exception("Only 'X' and 'O' are valid marks")
    for i in range(0, bo.shape[0]):
        if not np.in1d(bo[i, :], ['X', 'O', '']).all():
            raise Exception("Only 'X' and 'O' are valid marks")
    
def isWinner(bo, le):
    # Given a board state and the current player's letter, the function checks if the player has won    
    displayBoard(bo)       
    _validate(bo, le)        
    if _checkRows(bo, le) or _checkColumns(bo, le) or _checkDiagonals(bo, le):
        return True
    else:
        return False
